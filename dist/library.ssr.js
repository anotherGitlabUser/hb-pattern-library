'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var vue = require('vue');

var script$1 = vue.defineComponent({
    name: 'HB-Button',
    props: {
        disabled: {
            type: Boolean,
            default: false,
        },
        as: {
            type: [Object, String],
            default: 'button',
        },
        primary: {
            type: Boolean,
            default: false,
        },
        secondary: {
            type: Boolean,
            default: false,
        },
        label: {
            type: String,
            default: undefined,
        },
    },
    setup(properties) {
    },
});

function render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createBlock(vue.resolveDynamicComponent(_ctx.as), {
    class: vue.normalizeClass([
...(_ctx.primary ? ['primary'] : []),
...(_ctx.secondary ? ['secondary'] : [])
  ])
  }, {
    default: vue.withCtx(() => [
      vue.createTextVNode(vue.toDisplayString(_ctx.label) + " ", 1 /* TEXT */),
      vue.renderSlot(_ctx.$slots, "label"),
      vue.renderSlot(_ctx.$slots, "icon")
    ]),
    _: 3 /* FORWARDED */
  }, 8 /* PROPS */, ["class"]))
}

script$1.render = render$1;
script$1.__file = "src/components/button/Button.vue";

var script = vue.defineComponent({
    name: 'HB-Card',
    props: {
        border: {
            type: Boolean,
            default: true,
        },
        title: {
            type: String,
            default: undefined,
        },
        content: {
            type: String,
            default: undefined
        }
    },
    setup(properties) {
    },
});

function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (vue.openBlock(), vue.createElementBlock("div", {
    class: vue.normalizeClass(["card", [
...(_ctx.border ? ['card__border'] : []),
  ]])
  }, [
    vue.createElementVNode("span", null, vue.toDisplayString(_ctx.title), 1 /* TEXT */)
  ], 2 /* CLASS */))
}

script.render = render;
script.__file = "src/components/card/Card.vue";

exports.HbButton = script$1;
exports.HbCard = script;
