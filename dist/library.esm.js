import { defineComponent, openBlock, createBlock, resolveDynamicComponent, normalizeClass, withCtx, createTextVNode, toDisplayString, renderSlot, createElementBlock, createElementVNode } from 'vue';

var script$1 = defineComponent({
    name: 'HB-Button',
    props: {
        disabled: {
            type: Boolean,
            default: false,
        },
        as: {
            type: [Object, String],
            default: 'button',
        },
        primary: {
            type: Boolean,
            default: false,
        },
        secondary: {
            type: Boolean,
            default: false,
        },
        label: {
            type: String,
            default: undefined,
        },
    },
    setup(properties) {
    },
});

function render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return (openBlock(), createBlock(resolveDynamicComponent(_ctx.as), {
    class: normalizeClass([
...(_ctx.primary ? ['primary'] : []),
...(_ctx.secondary ? ['secondary'] : [])
  ])
  }, {
    default: withCtx(() => [
      createTextVNode(toDisplayString(_ctx.label) + " ", 1 /* TEXT */),
      renderSlot(_ctx.$slots, "label"),
      renderSlot(_ctx.$slots, "icon")
    ]),
    _: 3 /* FORWARDED */
  }, 8 /* PROPS */, ["class"]))
}

script$1.render = render$1;
script$1.__file = "src/components/button/Button.vue";

var script = defineComponent({
    name: 'HB-Card',
    props: {
        border: {
            type: Boolean,
            default: true,
        },
        title: {
            type: String,
            default: undefined,
        },
        content: {
            type: String,
            default: undefined
        }
    },
    setup(properties) {
    },
});

function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (openBlock(), createElementBlock("div", {
    class: normalizeClass(["card", [
...(_ctx.border ? ['card__border'] : []),
  ]])
  }, [
    createElementVNode("span", null, toDisplayString(_ctx.title), 1 /* TEXT */)
  ], 2 /* CLASS */))
}

script.render = render;
script.__file = "src/components/card/Card.vue";

export { script$1 as HbButton, script as HbCard };
