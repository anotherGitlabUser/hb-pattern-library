// import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import resolve from '@rollup/plugin-node-resolve';
// import commonjs from '@rollup/plugin-commonjs';

// https://blog.harveydelaney.com/creating-your-own-vue-component-library/
import typescript from 'rollup-plugin-typescript2';
import vue from 'rollup-plugin-vue';

import filesize from 'rollup-plugin-filesize';
import { terser } from 'rollup-plugin-terser';
import packageJson from './package.json';

export default [{
  input: 'src/index.ts',
  output: [
    /* {
      format: 'cjs',
      file: packageJson.main,
      sourcemap: true,
    }, */
    {
      format: 'esm',
      // file: packageJson.module,
      file: 'dist/index.esm.js',
      exports: 'named',
      // plugins: [terser()],
    },
    
  ],
  // plugins: [peerDepsExternal(), resolve(), commonjs(), typescript(), vue()],
  // plugins: [resolve(), typescript(), vue(), filesize()],

  external: ['vue'],
  plugins: [
    typescript(),
    vue(),
    filesize(),
  ]
},
 // SSR build.
 {
  input: 'src/index.ts',
  output: {
    format: 'cjs',
    file: 'dist/index.ssr.js'
  },
  external: ['vue'],
  plugins: [
    typescript(),
    vue(),
    filesize(),
  ]
}];
